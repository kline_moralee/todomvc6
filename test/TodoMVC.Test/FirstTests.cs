﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace TodoMVC.Test
{
    public class FirstTests
    {
        [Fact]
        public void It_passes()
        {
            Assert.Equal(2, Add(1, 1));
        }

        private int Add (int a, int b)
        {
            return a + b;
        }
    }
}
